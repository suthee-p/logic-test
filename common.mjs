if (process.argv.length !== 2) {
  console.log('usage: node --experimental-modules common.mjs')
  process.exit()
}

import util from './util.mjs'

function common (input) {
  return '';
};

util.track(() => {
  console.log(common(util.random_1000))
})
